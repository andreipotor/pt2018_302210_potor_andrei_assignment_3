package bll;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import dao.ClientDAO;
import model.Client;

public class ClientBLL {

	public ClientBLL() {
		
	}

	public Client findClientById(int id) {
		Client st = ClientDAO.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The Client with id =" + id + " was not found!");
		}
		return st;
	}
	
	public ArrayList<Object> getAll() {
		ArrayList<Object> st = ClientDAO.getAll();
		if (st == null) {
			throw new NoSuchElementException("couldn't get all clients\n");
		}
		return st;
	}

	public int insertClient(Client Client) {
		return ClientDAO.insert(Client);
	}
	
	public void deleteClient(int clientID) {
		ClientDAO.delete(clientID);
	}
}
