package bll;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import dao.HorderDAO;
import model.Horder;

public class HorderBLL {

	public HorderBLL() {
		
	}
	
	public ArrayList<Object> getAll() {
		ArrayList<Object> st = HorderDAO.getAll();
		if (st == null) {
			throw new NoSuchElementException("couldn't get all Horders\n");
		}
		return st;
	}

	public Horder findHorderById(int id) {
		Horder st = HorderDAO.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The Horder with id =" + id + " was not found!");
		}
		return st;
	}

	public int insertHorder(Horder Horder) {
		return HorderDAO.insert(Horder);
	}
	
	public void deleteHorder(int horderId){
		HorderDAO.delete(horderId);
	}
}
