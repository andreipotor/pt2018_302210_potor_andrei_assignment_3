package bll;

import java.util.ArrayList;
import java.util.NoSuchElementException;

import dao.ProductDAO;
import model.Product;

public class ProductBLL {

	public ProductBLL() {
		
	}
	
	public ArrayList<Object> getAll() {
		ArrayList<Object> st = ProductDAO.getAll();
		if (st == null) {
			throw new NoSuchElementException("couldn't get all Products\n");
		}
		return st;
	}

	public Product findProductById(int id) {
		Product st = ProductDAO.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The Product with id =" + id + " was not found!");
		}
		return st;
	}

	public int insertProduct(Product Product) {
		return ProductDAO.insert(Product);
	}
	
	public void deleteProduct(int productID) {
		ProductDAO.delete(productID);
	}
	
	public void updateProductQuantity(int newQty, int productID){
		ProductDAO.updateQuantity(newQty, productID);
	}
}
