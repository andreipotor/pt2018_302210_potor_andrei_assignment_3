package start;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import bll.ClientBLL;
import bll.HorderBLL;
import bll.ProductBLL;
import model.Client;
import model.Horder;
import model.Product;

public class Start {
	protected static final Logger LOGGER = Logger.getLogger(Start.class.getName());
	private static int width = 1200, height = 700;
	private static ClientBLL ClientBll = new ClientBLL();
	private static ProductBLL ProductBll = new ProductBLL();
	private static HorderBLL HorderBll = new HorderBLL();
	private static JTable clientTable,productTable,orderTable;
	private static JScrollPane jsp,jsp2,jsp3;
	private static JFrame frame;
	private static JPanel panel;
	private static JButton clientAdd, clientDelete, productAdd, productDelete, orderAdd, orderDelete;
	private static ActionListener bl;
	private static JTextField t1, t2, t3;
	private static int receiptNr = 1;
	private static JLabel l1,l2,l3;
	
	private static String[] getFieldNames(Class c){
		String[] ret = new String[c.getDeclaredFields().length];
		int i = 0;
		for(Field field: c.getDeclaredFields()){
			field.setAccessible(true);
			try {
				ret[i++] = capitalize(field.getName());
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			}
		}
		return ret;
	}
	
	private static String capitalize(String s){
		String ret = "";
		if((s!=null) && (s.length() > 0) && (Character.isLetter(s.charAt(0)))){
			ret += Character.toUpperCase(s.charAt(0));
			if(s.length() > 1)
				ret += s.substring(1);
		}
		return ret;
	}
	
	private static JTable createTable(ArrayList<Object> objects){
		String[] columnNames = getFieldNames(objects.get(0).getClass());
		
		int nrFields = objects.get(0).getClass().getDeclaredFields().length;
		Object rowData[][] = new Object[objects.size()][nrFields];
		for(int i = 0; i<objects.size(); i++){
			for(int j = 0; j<nrFields; j++){
				try {
					Method getter = objects.get(0).getClass().getMethod("get" + capitalize(columnNames[j]), null);
					getter.setAccessible(true);
					try {
						rowData[i][j] = getter.invoke(objects.get(i), null);
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (InvocationTargetException e) {
						e.printStackTrace();
					}
				} catch (NoSuchMethodException e) {
					e.printStackTrace();
				} catch (SecurityException e) {
					e.printStackTrace();
				}
			}
		}
		return new JTable(rowData, columnNames);
	}
	
	private static void updateTable1(){
		clientTable = createTable(ClientBll.getAll());
		jsp.setViewportView(clientTable);
	    
	}
	
	private static void updateTable2(){
	    productTable = createTable(ProductBll.getAll());
		jsp2.setViewportView(productTable);
	}
	
	private static void updateTable3(){
	    orderTable = createTable(HorderBll.getAll());
		jsp3.setViewportView(orderTable);
	}
	
	private static void updateTables(){
		updateTable1();
		updateTable2();
		updateTable3();
	}

	private static void addClientButton(){
		try {
			ClientBll.insertClient(new Client(t1.getText(), t2.getText()));
			updateTables();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
	
	private static void deleteClientButton(){
		try {
			ClientBll.deleteClient(Integer.parseInt(t1.getText()));
			updateTables();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
	
	private static void addProductButton(){
		try {
			ProductBll.insertProduct(new Product(t1.getText(), Integer.parseInt(t2.getText()) , Float.parseFloat(t3.getText()) ));
			updateTables();
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
		}
	}
	
	private static void deleteProductButton(){
		try {
			ProductBll.deleteProduct(Integer.parseInt(t1.getText()));
			updateTables();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
	
	private static void makeOrderButton(){
		try {
			int quantity = Integer.parseInt(t1.getText());
			int clientID = Integer.parseInt(t2.getText());
			int productID = Integer.parseInt(t3.getText());
			Product p = ProductBll.findProductById(productID);
			if(p.getQuantity() - quantity >= 0){
				ProductBll.updateProductQuantity(p.getQuantity() - quantity, productID);
				HorderBll.insertHorder(new Horder(quantity, clientID,  productID));	
				makeReceipt(quantity, clientID, productID);
				updateTables();
			} else {
				System.out.println("Not enough products in stock.");
			}
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
		}
	}
	
	private static void deleteOrderButton(){
		try {
			HorderBll.deleteHorder(Integer.parseInt(t1.getText()));
			updateTables();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
	
	private static void initButtonListener(){
		bl = new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(((JButton)(e.getSource())).getText() == "Add client"){
					addClientButton();
				}else
				if(((JButton)(e.getSource())).getText() == "Delete client"){
					deleteClientButton();
				}else
				if(((JButton)(e.getSource())).getText() == "Add product"){
					addProductButton();
				}else
				if(((JButton)(e.getSource())).getText() == "Delete product"){
					deleteProductButton();
				}else
				if(((JButton)(e.getSource())).getText() == "MAKE ORDER"){
					makeOrderButton();
				}else
				if(((JButton)(e.getSource())).getText() == "Delete order"){
					deleteOrderButton();
				}
			}
		};
	}
	
	private static void initTable1(){
		clientTable = new JTable();
	    clientTable.setBounds(0, 30, width/3 - 5, (int)(height * 0.65f));
		jsp = new JScrollPane();
		jsp.setViewportView(clientTable);
		jsp.setBounds(clientTable.getBounds());
		panel.add(jsp);
	}
	
	private static void initTable2(){
		productTable = new JTable();
		productTable.setBounds(width/3,30,width/3 - 5,(int)(height * 0.65f));
		jsp2 = new JScrollPane();
		jsp2.setViewportView(productTable);
		jsp2.setBounds(productTable.getBounds());
		panel.add(jsp2);
	}
	
	private static void initTable3(){
		orderTable = new JTable();
		orderTable.setBounds(2*width/3,30,width/3 - 5,(int)(height * 0.65f));
		jsp3 = new JScrollPane();
		jsp3.setViewportView(orderTable);
		jsp3.setBounds(orderTable.getBounds());
		panel.add(jsp3);
	}
	
	private static void initUI(){
		
		initButtonListener();
		
		frame = new JFrame("Database - Tema 3 - Potor Dan Andrei 302210");
		frame.setSize(width, height);
		frame.setResizable(false);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(null);
		
		panel = new JPanel();
		panel.setSize(width, height);
		panel.setLocation(0, 0);
		panel.setLayout(null);
		
		// initialize tables
		initTable1();
		initTable2();
		initTable3();
		updateTables();
		
		// BUTTONS
		clientAdd = new JButton("Add client");
		clientAdd.setBounds(0 + 3, (int)(height * 0.65f) + 50, width/6 - 10, height/20);
		clientAdd.addActionListener(bl);
		panel.add(clientAdd);
		
		clientDelete = new JButton("Delete client");
		clientDelete.setBounds(width/6 + 3, (int)(height * 0.65f) + 50, width/6 - 10, height/20);
		clientDelete.addActionListener(bl);
		panel.add(clientDelete);
		
		productAdd = new JButton("Add product");
		productAdd.setBounds(2 * width/6 + 3, (int)(height * 0.65f) + 50, width/6 - 10, height/20);
		productAdd.addActionListener(bl);
		panel.add(productAdd);
		
		productDelete = new JButton("Delete product");
		productDelete.setBounds(3 * width/6 + 3, (int)(height * 0.65f) + 50, width/6 - 10, height/20);
		productDelete.addActionListener(bl);
		panel.add(productDelete);
		
		orderAdd = new JButton("MAKE ORDER");
		orderAdd.setBackground(Color.YELLOW);
		orderAdd.setBounds(4 * width/6 + 3, (int)(height * 0.65f) + 50, width/6 - 10, height/20);
		orderAdd.addActionListener(bl);
		panel.add(orderAdd);
		
		orderDelete = new JButton("Delete order");
		orderDelete.setBounds(5 * width/6 + 3, (int)(height * 0.65f) + 50, width/6 - 10, height/20);
		orderDelete.addActionListener(bl);
		panel.add(orderDelete);
		
		// TEXTBOXES
		t1 = new JTextField();
		t1.setBounds(0 + 3, (int)(height * 0.65f) + 100, width/3 - 10, height/20);
		panel.add(t1);
		
		t2 = new JTextField();
		t2.setBounds(width/3 + 3, (int)(height * 0.65f) + 100, width/3 - 10, height/20);
		panel.add(t2);
		
		t3 = new JTextField();
		t3.setBounds(2*width/3 + 3, (int)(height * 0.65f) + 100, width/3 - 10, height/20);
		panel.add(t3);
		
		// LABELS
		l1 = new JLabel();
		l1.setBounds(0 + 6, (int)(height * 0.65f) + 130, width/3 - 10, height/20);
		l1.setText("Column 1 - Also used as ID by delete");
		panel.add(l1);
		
		l2 = new JLabel();
		l2.setBounds(width/3 + 6, (int)(height * 0.65f) + 130, width/3 - 10, height/20);
		l2.setText("Column 2");
		panel.add(l2);
		
		l3 = new JLabel();
		l3.setBounds(2*width/3 + 6, (int)(height * 0.65f) + 130, width/3 - 10, height/20);
		l3.setText("Column 3");
		panel.add(l3);
		
		frame.add(panel);
		frame.setVisible(true);
	}
	
	private static void makeReceipt(int quantity, int clientID, int productID){
		String s = "";
		Client c = ClientBll.findClientById(clientID);
		Product p = ProductBll.findProductById(productID);
		
		s =  "Client ID:    \"" + c.getClientID() + "\"\n";
		s += "Client name:  \"" + c.getName() + "\"\n";
		s += "Client email: \"" + c.getEmail() + "\"\n\n";
		
		s += "Product ID:    \"" + p.getProductID() + "\"\n";
		s += "Product name:  \"" + p.getName() + "\"\n";
		s += "Product price: \"" + p.getPrice() + "\"\n\n";
		
		s += "Quantity ordered: " + quantity + "\n\n";
		
		s += "Total price: " + (quantity * p.getPrice()) + "\n\n";
		
		Path file = Paths.get("./receipt" + receiptNr + ".txt");
		receiptNr++;
		
		Charset charset = Charset.forName("US-ASCII");
		try (BufferedWriter writer = Files.newBufferedWriter(file, charset)) {
			writer.write(s, 0, s.length());
		} catch (IOException x) {
		    System.err.format("IOException: %s%n", x);
		}

	}
	
	public static void main(String[] args) throws SQLException {
		initUI();
	}

}
