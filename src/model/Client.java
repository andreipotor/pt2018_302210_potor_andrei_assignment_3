package model;

public class Client{
	private int clientID;
	private String name;
	private String email;

	public Client(int clientID, String name, String email) {
		super();
		this.clientID = clientID;
		this.name = name;
		this.email = email;
	}

	public Client(String name, String email) {
		super();
		this.name = name;
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "Client [clientID=" + clientID + ", name=" + name +", email=" + email
				+ "]";
	}

	public int getClientID() {
		return clientID;
	}

	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
