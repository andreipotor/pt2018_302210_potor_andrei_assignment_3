package model;

public class Horder {
	private int HorderID;
	private int quantity;
	private int clientID;
	private int productID;

	public Horder(int HorderID, int quantity, int clientID, int productID) {
		super();
		this.HorderID = HorderID;
		this.quantity = quantity;
		this.clientID = clientID;
		this.productID = productID;
	}

	public Horder(int quantity, int clientID, int productID) {
		super();
		this.quantity = quantity;
		this.clientID = clientID;
		this.productID = productID;
	}

	@Override
	public String toString() {
		return "Horder [HorderID=" + HorderID + ", quantity=" + quantity  + ", clientID=" + clientID  + ", productID=" + productID
				+ "]";
	}

	public int getHorderID() {
		return HorderID;
	}

	public void setHorderID(int HorderID) {
		this.HorderID = HorderID;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getClientID() {
		return clientID;
	}

	public void setClientID(int clientID) {
		this.clientID = clientID;
	}

	public int getProductID() {
		return productID;
	}

	public void setProductID(int productID) {
		this.productID = productID;
	}

}
