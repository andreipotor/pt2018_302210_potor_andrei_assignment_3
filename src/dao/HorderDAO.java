package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import model.Horder;

public class HorderDAO {

	protected static final Logger LOGGER = Logger.getLogger(HorderDAO.class.getName());
	private static final String insertStatementString = "INSERT INTO horder (quantity,clientID,productID)"
			+ " VALUES (?,?,?)";
	private final static String findStatementString = "SELECT * FROM horder where HorderID = ?";

	public static Horder findById(int horderId) {
		Horder toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, horderId);
			rs = findStatement.executeQuery();
			rs.next();

			int quantity = rs.getInt("quantity");
			int clientID = rs.getInt("clientID");
			int productID = rs.getInt("productID");
			
			toReturn = new Horder(horderId, quantity, clientID, productID);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"HorderDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	public static ArrayList<Object> getAll() {
		ArrayList<Object> res = new ArrayList<Object>();

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		
		ResultSet rs = null;
		try {
			String statement = "SELECT * FROM horder";
			findStatement = dbConnection.prepareStatement(statement);
			rs = findStatement.executeQuery();
			while(rs.next() != false){	
				int HorderID = rs.getInt("HorderID");
				int quantity = rs.getInt("quantity");
				int clientID = rs.getInt("clientID");
				int productID = rs.getInt("productID");
				
				res.add(new Horder(HorderID,quantity,clientID,productID));
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return res;
	}

	public static int insert(Horder Horder) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setInt(1, Horder.getQuantity());
			insertStatement.setInt(2, Horder.getClientID());
			insertStatement.setInt(3, Horder.getProductID());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "HorderDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	
	public static void delete(int horderId) {
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		try {
			String statement = "DELETE FROM horder WHERE horderID=?";
			findStatement = dbConnection.prepareStatement(statement);
			findStatement.setLong(1, horderId);
			findStatement.executeUpdate();
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"HorderDAO:delete " + e.getMessage());
		} finally {
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
	}
}
